﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void ARTapToPlaceObject::Start()
extern void ARTapToPlaceObject_Start_mCEA5A9ADC5ACD785F329254FD679C5513136EDD4 ();
// 0x00000002 System.Void ARTapToPlaceObject::Update()
extern void ARTapToPlaceObject_Update_mCEA015466DB37FE1DC8947F8B16E5773004E3F7C ();
// 0x00000003 System.Void ARTapToPlaceObject::PlaceObject()
extern void ARTapToPlaceObject_PlaceObject_m72557EDFCEA7F7FDBE5E40E5ADE990B91FCB2EA7 ();
// 0x00000004 System.Void ARTapToPlaceObject::UpdatePlacementIndicator()
extern void ARTapToPlaceObject_UpdatePlacementIndicator_m03B360FB4F2108EE4568BEDD76425EEB98FC2B9C ();
// 0x00000005 System.Void ARTapToPlaceObject::UpdatePlacementPose()
extern void ARTapToPlaceObject_UpdatePlacementPose_m97C991933A4B270119305A0974E6D69D45D6640D ();
// 0x00000006 System.Void ARTapToPlaceObject::.ctor()
extern void ARTapToPlaceObject__ctor_mF5B2CD917913D8081CB42BA882632868D038267B ();
// 0x00000007 System.Void UnityTemplateProjects.SimpleCameraController::OnEnable()
extern void SimpleCameraController_OnEnable_m23B1FC65DDEECCB31E2A64715EF11299D5FC671B ();
// 0x00000008 UnityEngine.Vector3 UnityTemplateProjects.SimpleCameraController::GetInputTranslationDirection()
extern void SimpleCameraController_GetInputTranslationDirection_m73C99DB69CEB467834BBA00A62415D1CEEF0CB47 ();
// 0x00000009 System.Void UnityTemplateProjects.SimpleCameraController::Update()
extern void SimpleCameraController_Update_m703B3BEF0A4BD9BCC6A89BA83CACEC85FB45CEA5 ();
// 0x0000000A System.Void UnityTemplateProjects.SimpleCameraController::.ctor()
extern void SimpleCameraController__ctor_mDD687DF4764B1C2C77BA64E8836650F6F31F144A ();
// 0x0000000B System.Void UnityTemplateProjects.SimpleCameraController_CameraState::SetFromTransform(UnityEngine.Transform)
extern void CameraState_SetFromTransform_mE0003493BE2BC151ECB65F8FA10FD87DD2D380E9 ();
// 0x0000000C System.Void UnityTemplateProjects.SimpleCameraController_CameraState::Translate(UnityEngine.Vector3)
extern void CameraState_Translate_m1B8281ABF2A3296F5DE47064F3757FD481E446EE ();
// 0x0000000D System.Void UnityTemplateProjects.SimpleCameraController_CameraState::LerpTowards(UnityTemplateProjects.SimpleCameraController_CameraState,System.Single,System.Single)
extern void CameraState_LerpTowards_mC4C84789CEE27DEC5A70AAF00FC71FE990519AFE ();
// 0x0000000E System.Void UnityTemplateProjects.SimpleCameraController_CameraState::UpdateTransform(UnityEngine.Transform)
extern void CameraState_UpdateTransform_m405C51A0B661CF16A1A4F5A162529398E1DAC358 ();
// 0x0000000F System.Void UnityTemplateProjects.SimpleCameraController_CameraState::.ctor()
extern void CameraState__ctor_m8552062EB90FFEEA837B63DD22A9CB4F438EABDE ();
static Il2CppMethodPointer s_methodPointers[15] = 
{
	ARTapToPlaceObject_Start_mCEA5A9ADC5ACD785F329254FD679C5513136EDD4,
	ARTapToPlaceObject_Update_mCEA015466DB37FE1DC8947F8B16E5773004E3F7C,
	ARTapToPlaceObject_PlaceObject_m72557EDFCEA7F7FDBE5E40E5ADE990B91FCB2EA7,
	ARTapToPlaceObject_UpdatePlacementIndicator_m03B360FB4F2108EE4568BEDD76425EEB98FC2B9C,
	ARTapToPlaceObject_UpdatePlacementPose_m97C991933A4B270119305A0974E6D69D45D6640D,
	ARTapToPlaceObject__ctor_mF5B2CD917913D8081CB42BA882632868D038267B,
	SimpleCameraController_OnEnable_m23B1FC65DDEECCB31E2A64715EF11299D5FC671B,
	SimpleCameraController_GetInputTranslationDirection_m73C99DB69CEB467834BBA00A62415D1CEEF0CB47,
	SimpleCameraController_Update_m703B3BEF0A4BD9BCC6A89BA83CACEC85FB45CEA5,
	SimpleCameraController__ctor_mDD687DF4764B1C2C77BA64E8836650F6F31F144A,
	CameraState_SetFromTransform_mE0003493BE2BC151ECB65F8FA10FD87DD2D380E9,
	CameraState_Translate_m1B8281ABF2A3296F5DE47064F3757FD481E446EE,
	CameraState_LerpTowards_mC4C84789CEE27DEC5A70AAF00FC71FE990519AFE,
	CameraState_UpdateTransform_m405C51A0B661CF16A1A4F5A162529398E1DAC358,
	CameraState__ctor_m8552062EB90FFEEA837B63DD22A9CB4F438EABDE,
};
static const int32_t s_InvokerIndices[15] = 
{
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1123,
	23,
	23,
	26,
	1254,
	1231,
	26,
	23,
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	15,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
