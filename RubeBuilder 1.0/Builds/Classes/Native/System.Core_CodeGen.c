﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 ();
// 0x00000002 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 ();
// 0x00000003 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000004 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000005 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000006 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000007 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000008 System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x00000009 TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x0000000A System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x0000000B System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x0000000C System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x0000000D System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x0000000E System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000000F System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x00000010 System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000011 System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x00000012 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000013 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x00000014 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x00000015 System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x00000016 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000017 System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x00000018 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x00000019 System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x0000001A System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000001B System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000001C System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x0000001D System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x0000001E System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000001F System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x00000020 System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
static Il2CppMethodPointer s_methodPointers[32] = 
{
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[32] = 
{
	0,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[10] = 
{
	{ 0x02000004, { 22, 4 } },
	{ 0x02000005, { 26, 9 } },
	{ 0x02000006, { 35, 7 } },
	{ 0x02000007, { 42, 10 } },
	{ 0x02000008, { 52, 1 } },
	{ 0x06000003, { 0, 10 } },
	{ 0x06000004, { 10, 5 } },
	{ 0x06000005, { 15, 3 } },
	{ 0x06000006, { 18, 1 } },
	{ 0x06000007, { 19, 3 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[53] = 
{
	{ (Il2CppRGCTXDataType)2, 12761 },
	{ (Il2CppRGCTXDataType)3, 9900 },
	{ (Il2CppRGCTXDataType)2, 12762 },
	{ (Il2CppRGCTXDataType)2, 12763 },
	{ (Il2CppRGCTXDataType)3, 9901 },
	{ (Il2CppRGCTXDataType)2, 12764 },
	{ (Il2CppRGCTXDataType)2, 12765 },
	{ (Il2CppRGCTXDataType)3, 9902 },
	{ (Il2CppRGCTXDataType)2, 12766 },
	{ (Il2CppRGCTXDataType)3, 9903 },
	{ (Il2CppRGCTXDataType)2, 12767 },
	{ (Il2CppRGCTXDataType)3, 9904 },
	{ (Il2CppRGCTXDataType)3, 9905 },
	{ (Il2CppRGCTXDataType)2, 10596 },
	{ (Il2CppRGCTXDataType)3, 9906 },
	{ (Il2CppRGCTXDataType)2, 10598 },
	{ (Il2CppRGCTXDataType)2, 12768 },
	{ (Il2CppRGCTXDataType)3, 9907 },
	{ (Il2CppRGCTXDataType)2, 10601 },
	{ (Il2CppRGCTXDataType)2, 10603 },
	{ (Il2CppRGCTXDataType)2, 12769 },
	{ (Il2CppRGCTXDataType)3, 9908 },
	{ (Il2CppRGCTXDataType)3, 9909 },
	{ (Il2CppRGCTXDataType)3, 9910 },
	{ (Il2CppRGCTXDataType)2, 10608 },
	{ (Il2CppRGCTXDataType)3, 9911 },
	{ (Il2CppRGCTXDataType)3, 9912 },
	{ (Il2CppRGCTXDataType)2, 10617 },
	{ (Il2CppRGCTXDataType)2, 12770 },
	{ (Il2CppRGCTXDataType)3, 9913 },
	{ (Il2CppRGCTXDataType)3, 9914 },
	{ (Il2CppRGCTXDataType)2, 10619 },
	{ (Il2CppRGCTXDataType)2, 12669 },
	{ (Il2CppRGCTXDataType)3, 9915 },
	{ (Il2CppRGCTXDataType)3, 9916 },
	{ (Il2CppRGCTXDataType)3, 9917 },
	{ (Il2CppRGCTXDataType)2, 10626 },
	{ (Il2CppRGCTXDataType)2, 12771 },
	{ (Il2CppRGCTXDataType)3, 9918 },
	{ (Il2CppRGCTXDataType)3, 9919 },
	{ (Il2CppRGCTXDataType)3, 9484 },
	{ (Il2CppRGCTXDataType)3, 9920 },
	{ (Il2CppRGCTXDataType)3, 9921 },
	{ (Il2CppRGCTXDataType)2, 10635 },
	{ (Il2CppRGCTXDataType)2, 12772 },
	{ (Il2CppRGCTXDataType)3, 9922 },
	{ (Il2CppRGCTXDataType)3, 9923 },
	{ (Il2CppRGCTXDataType)3, 9924 },
	{ (Il2CppRGCTXDataType)3, 9925 },
	{ (Il2CppRGCTXDataType)3, 9926 },
	{ (Il2CppRGCTXDataType)3, 9490 },
	{ (Il2CppRGCTXDataType)3, 9927 },
	{ (Il2CppRGCTXDataType)3, 9928 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	32,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	10,
	s_rgctxIndices,
	53,
	s_rgctxValues,
	NULL,
};
